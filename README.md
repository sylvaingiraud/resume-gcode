# Resume-gcode

Clean up a gcode file of 3D printing to resume the printing at a specified vertical position.
Used after a failure, to resume the printing.
Important: do not remove the object after the failure in order to keep the exact position in the printer.

# You need:
* To measure Z at the time of the failure
* The original gcode file 

Run his way:

    ./resume_gcode.sh <nom du fuchier gcode d'origine> <valeur Z mesurée>

# Usage: 

    resume-gcode.sh <original gcode file> <Z value where to resume (mm)> 

    Anything higher than Z will be kept. Everything else is dropped.

# Example for a failure at 3.1 mm:

    resume-gcode.sh yogcapv6small.gcode 3.1

The script shows removed lines numbers:
    
    Done. Removed lines from 45 to 5348

    Result in yogcapv6small.gcode-resume3.1.gcode 

# Recommandations
* Display the Travel Moves in the software to check there is nothing wrong, specially that the head doesn't go too low.
* Reduce speed on printing start to allow emergency interruption if moves are incorrect. 

# Limitations
Tested with Repetier-Host 2.1.6 ocnnected to a Prusa i3

Not tested with other software or printers

!!! No guarantee at all !!
